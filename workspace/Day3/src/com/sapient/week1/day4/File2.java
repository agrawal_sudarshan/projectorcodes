package com.sapient.week1.day4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class File2 {
public static void main(String[] args) {
	try {
		Scanner sc = new Scanner(System.in);
		BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\sudagraw\\Documents\\file3.txt"));
		BufferedWriter bw = new BufferedWriter(new FileWriter("C:\\Users\\sudagraw\\Documents\\file4.txt"));
		String line ;
		String word;
		System.out.println("Enter the key word");
		word = sc.nextLine();
		while((line=br.readLine())!=null) {
			if(line.contains(word)) {
				bw.write(line);
				bw.newLine();
			}
		}
		br.close();
		bw.close();
		sc.close();
	}
	catch(IOException e) {
		
	}
	catch(Exception e) {
		
	}
	catch(Throwable e) {
		
	}

	
}
}
