package com.sapient.week1.day4;

public class Ticktock implements Runnable{
    private Thread currentThread;
    private static final int N = 3;
    private String value;
    private static Object obj = new Object();
    
    public Ticktock(String value) {
           this.value = value;
           currentThread = new Thread(this);
           currentThread.start();
    }
    public String getValue() {
           return value;
    }
    public void setValue(String value) {
           this.value = value;
    }
    @Override
    public void run() {
           for(int counter=0; counter<N; counter++) {
                  synchronized (obj) {
                        System.out.println(this.getValue());
                        obj.notify();
                        try {
                        	Thread.sleep(1000);
                               obj.wait();
                        } catch (InterruptedException e) {
                               e.printStackTrace();
                        }      
                  }
           }
    }
    public static void main(String[] args) {
		new Ticktock("Tick");
		new Ticktock("Tock");
	}
}
