package com.sapient.week1.day4;

public class Demo {

	public static void main(String[] args) {
		EmployeeData ed =  new EmployeeData();
		ed.insertData(new Employee("Sudarshan", 21,01));
		ed.insertData(new Employee("Siddhant", 22,02));
		ed.insertData(new Employee("Rajat", 18,03));
		ed.insertData(new Employee("Chinmay", 25,04));
		ed.insertData(new Employee("Ajay", 31,05));
		for(Employee emp:ed.getList(1)) {
			System.out.println(emp);
		}
	}

}
