package com.sapient.week1.day4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class File {
	public static void main(String[] args) {
		try {
		BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\sudagraw\\Documents\\file.csv"));
		BufferedWriter bw = new BufferedWriter(new FileWriter("C:\\Users\\sudagraw\\Documents\\file1.csv")); 
		String line;
		while((line=br.readLine())!=null) {
			String[] s1 = line.split(", ");
			int sum = 0;
			sum = Arrays.stream(s1).mapToInt(num->Integer.parseInt(num.trim())).sum();
			line = line + " = " + sum;
			bw.write(line);
			bw.newLine();
		}
		br.close();
		bw.close();
		System.out.println("File is processed");
		}
		catch(IOException e) {
			System.out.println("No file present");
		}
		catch(Exception e) {
			
		}
		catch(Throwable e) {
			
		}
	}
}
