package com.sapient.week1;

import java.util.ArrayList;

public class Duplicate {	
	private ArrayList<Object> arr = new ArrayList<Object>();
	public void add(Object ob) {
		arr.add(ob);
	}
	public void remove() {
		ArrayList<Object> temp = new ArrayList<Object>();
		arr.stream().distinct().forEach(e->temp.add(e));
		temp.forEach(System.out::println);
	}
	public static void main(String[] args) {
		Duplicate dup = new Duplicate();
		dup.add(1);
		dup.add(1);
		dup.add(2);
		dup.add(2);
		dup.add(3);
		dup.add("Sudarshan");
		dup.add("Sudarshan");
		dup.remove();
	}
}
